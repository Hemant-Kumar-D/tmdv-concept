package com.example.allapi_call

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer

import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.allapi_call.Adapter.UserAdpter
import com.example.allapi_call.ApiRepogetry.UserRepogetry
import com.example.allapi_call.Factroy.UserFactroy
import com.example.allapi_call.Viewmodel.Userviewmodel
import com.example.allapi_call.databinding.ActivityMainBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var binding:ActivityMainBinding
    lateinit var factroy:UserFactroy
    lateinit var viewModel: Userviewmodel

    lateinit var adapter:UserAdpter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=DataBindingUtil.setContentView(this,R.layout.activity_main)
        factroy= UserFactroy(UserRepogetry(UaerDatabase.Contactatabse(this).userdao(),"4da2ce878649bbad5b1ad09442ef2095"))
        viewModel=ViewModelProvider(this,factroy)[Userviewmodel::class.java]
        binding.lifecycleOwner=this
        binding.userRecycilerview.layoutManager=LinearLayoutManager(this)
        CoroutineScope(Dispatchers.Main).launch {
            viewModel.getmovie().observe(this@MainActivity, Observer {
                adapter = UserAdpter(it,this@MainActivity)
                binding.userRecycilerview.adapter = adapter
                adapter.notifyDataSetChanged()
            })
        }

    }
}