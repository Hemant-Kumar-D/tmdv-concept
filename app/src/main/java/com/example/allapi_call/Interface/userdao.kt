package com.example.allapi_call.Interface

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.Companion.REPLACE
import androidx.room.Query
import com.example.allapi_call.Model.Movie


@Dao
 interface  userdao {
    @Insert(onConflict = REPLACE)
    suspend fun insertMovieindb(user: Movie)
    @Query("SELECT*FROM Movie")
    fun getallmovietolacaldb(): List<Movie>



}