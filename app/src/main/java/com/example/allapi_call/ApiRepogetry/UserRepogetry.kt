package com.example.allapi_call.ApiRepogetry


import android.util.Log

import androidx.lifecycle.MutableLiveData

import com.example.allapi_call.AllApi.UserRetrofitclint
import com.example.allapi_call.Interface.userdao
import com.example.allapi_call.Model.Movie
import com.example.allapi_call.Responce.responce


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserRepogetry(private var userDao:userdao,private var api_key:String){
    var userdat:MutableLiveData<List<Movie>> = MutableLiveData()
    val TAG="Checkdata"
    fun getallmovie(): MutableLiveData<List<Movie>>{
        val call=UserRetrofitclint.user.getallmovies(api_key)
        call.enqueue(object:Callback<responce>{
            override fun onResponse(call: Call<responce>, response: Response<responce>) {
               if(response.isSuccessful){
                   var answer= response.body()
                 userdat.postValue(answer!!.moveilist)
                   CoroutineScope(Dispatchers.Main).launch {
                       answer.moveilist.let {
                           for (responce in it)
                               userDao.insertMovieindb(responce)
                       }
                   }
                   }
            }
            override fun onFailure(call: Call<responce>, t: Throwable) {
                Log.d(TAG, "onFailure:${t.message}")
            }

        })
        return userdat!!
    }
    fun getallmovietodb():List<Movie>{
       return userDao.getallmovietolacaldb()
    }
    suspend fun getmovie(): MutableLiveData<List<Movie>> {
        var getdata:List<Movie>?=null
        var job=CoroutineScope(Dispatchers.IO).async {
            getdata=getallmovietodb()
            return@async getdata
        }
        if (job.await()!!.isNotEmpty()){
            userdat.postValue(getdata!!)

        }
        else{
                userdat=getallmovie()
        }
        return userdat
    }
}



