package com.example.allapi_call

import android.content.Context
import android.net.ConnectivityManager
import androidx.core.content.ContextCompat


class Networkconnection {
    companion object {
        fun activeNetwork(context: Context): Boolean {
            val cm =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            val activeNetwork = cm!!.activeNetworkInfo
            return activeNetwork != null &&
                    activeNetwork.isConnected
        }

    }

}