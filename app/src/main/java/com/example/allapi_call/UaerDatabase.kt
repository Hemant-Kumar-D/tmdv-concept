package com.example.allapi_call

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.allapi_call.Interface.userdao
import com.example.allapi_call.Model.Movie

@Database(entities = [Movie::class], version = 2)
abstract class UaerDatabase(): RoomDatabase() {
    abstract fun userdao():userdao
    companion object {
        @Volatile
        var INTENCE:UaerDatabase? = null
        fun Contactatabse(context: Context): UaerDatabase {
            var instence=INTENCE
            if (INTENCE == null) {
                synchronized(this) {
                    instence =
                        Room.databaseBuilder(context, UaerDatabase::class.java, "UserData")
                            .build()
                    INTENCE=instence
                }

            }
            return INTENCE!!
        }
    }


}
