package com.example.allapi_call.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.allapi_call.Model.Movie
import com.example.allapi_call.R

import com.example.allapi_call.databinding.UserlayoutBinding
import com.squareup.picasso.Picasso

class UserAdpter(private val user:List<Movie>,private val context: Context): RecyclerView.Adapter<UserAdpter.MyViewHolder>() {

    inner class MyViewHolder(var binding:UserlayoutBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var view: View? = LayoutInflater.from(parent.context).inflate(R.layout.userlayout, parent, false)
        var binding: UserlayoutBinding = DataBindingUtil.bind(view!!)!!
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return user.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val album = user[position]

        holder.binding.apply {
           var uri_path="https://image.tmdb.org/t/p/w500/${album.poster_path}"
            Picasso.with(context).load(uri_path).into(imageView)

        }
    }

}