package com.example.allapi_call.AllApi


import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

private val baseurl:String="https://api.themoviedb.org/"
object UserRetrofitclint {
    val retrofitclient: Retrofit.Builder by lazy {
        Retrofit.Builder()
            .baseUrl(baseurl)
            .addConverterFactory(
                GsonConverterFactory
                    .create(GsonBuilder().create())
            )
    }


    val user:User by lazy {
        retrofitclient.build().create(User::class.java)
    }
}
