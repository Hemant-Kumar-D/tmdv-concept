package com.example.allapi_call.Factroy

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.allapi_call.ApiRepogetry.UserRepogetry
import com.example.allapi_call.Viewmodel.Userviewmodel

class UserFactroy(private val userrepo: UserRepogetry):ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(Userviewmodel::class.java)) {
            return Userviewmodel(userrepo) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}