package com.example.allapi_call.Model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
@Entity
data class Movie(
    @PrimaryKey(autoGenerate = true)
    var srno:Int,
    @SerializedName("backdrop_path")
    var Bakgroundposter:String,
    @SerializedName("first_air_date")
    var Launchdate:String,
    var id :Int,
    @SerializedName("name")
    var Movie_name:String,
    var original_language:String,
    var overview:String,
    var popularity:Float,
    var poster_path:String,
    var vote_average:Int,
    var vote_count:Int
    )
